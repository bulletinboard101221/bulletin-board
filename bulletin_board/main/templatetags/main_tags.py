from django import template
from django.db.models import Q
from main.models import *

register = template.Library()


@register.simple_tag
def get_selected_categories():
    return Category.objects.filter(
        Q(name__contains='Недвижимость') | Q(name__contains='Животные') | Q(name__contains='Техника'))



@register.simple_tag
def get_all_categories():
    return Category.objects.all()
