from rest_framework import serializers

from main.models import *


class AdvertSerializer(serializers.ModelSerializer):
    author = serializers.SlugRelatedField(slug_field='last_name', read_only=True)

    class Meta:
        model = Advert
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=100)
    slug = serializers.SlugField(max_length=100)

    class Meta:
        model = Category
        fields = ('name', 'slug', 'parent')
