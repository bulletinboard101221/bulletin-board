from django.contrib import admin
from .models import *


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'parent')
    list_display_links = ('id', 'name')
    search_fields = ('name',)
    ordering = ('id',)


class CategoryDescriptionAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)
    search_fields = ('name',)


class DescriptionValueAdmin(admin.ModelAdmin):
    list_display = ('id', 'description', 'advert')
    list_display_links = ('id', 'advert')
    search_fields = ('advert',)


class GalleryInline(admin.TabularInline):
    model = Gallery


@admin.register(Advert)
class AdvertAdmin(admin.ModelAdmin):
    exclude = ('likes_qty',)
    list_display = ('id', 'title', 'author', 'category',)
    list_display_links = ('id', 'title', 'author',)
    inlines = [GalleryInline, ]


class SubscribersAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'email')
    list_display_links = ('id', 'first_name', 'email')
    search_fields = ('email',)
    ordering = ('id',)


admin.site.register(Category, CategoryAdmin, )
admin.site.register(Gallery, )
admin.site.register(DescriptionValue, DescriptionValueAdmin)
admin.site.register(CategoryDescription, CategoryDescriptionAdmin)
admin.site.register(Subscribers, SubscribersAdmin)
