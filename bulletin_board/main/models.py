from django.db import models
from users.models import CustomUser
from mptt.models import MPTTModel, TreeForeignKey


class Category(MPTTModel):
    name = models.CharField(max_length=100, verbose_name='Категория')
    slug = models.SlugField(max_length=100, verbose_name='Url')
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children',
                            verbose_name='Родительская категория')

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return '%s -> %s' % (self.parent.name, self.name) if self.parent else self.name


class Advert(models.Model):
    author = models.ForeignKey(CustomUser, on_delete=models.CASCADE, verbose_name='Автор')
    title = models.CharField(max_length=100, verbose_name='Название')
    category = models.ForeignKey(Category, on_delete=models.PROTECT, verbose_name='Категория')
    content = models.TextField(verbose_name='Описание объявления')
    price = models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Цена')
    publication_datetime = models.DateTimeField(auto_now_add=True, verbose_name='Дата публикации')
    edit_datetime = models.DateTimeField(auto_now=True, verbose_name='Дата изменения')
    address = models.CharField(max_length=200, verbose_name='Адрес')
    likes_qty = models.IntegerField(verbose_name='Кол-во лайков', blank=True, null=True)

    class Meta:
        verbose_name = 'Объявление'
        verbose_name_plural = 'Объявления'
        ordering = ['-publication_datetime']

    def __str__(self):
        return self.title


class Gallery(models.Model):
    advert = models.ForeignKey(Advert, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='adverts_images/%Y/%m/%d', verbose_name='Фото к объявлению')

    class Meta:
        verbose_name = 'Галерея'
        verbose_name_plural = 'Галереи'


class CategoryDescription(models.Model):
    category = models.ManyToManyField(Category)
    name = models.CharField(max_length=100, verbose_name='Характеристика категории')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Характеристика категории'
        verbose_name_plural = 'Характеристики категорий'


class DescriptionValue(models.Model):
    advert = models.ForeignKey(Advert, on_delete=models.CASCADE, verbose_name='Объявление')
    description = models.ForeignKey(CategoryDescription, on_delete=models.CASCADE, verbose_name='Характеристика')
    value = models.CharField(max_length=100, verbose_name='Значение характеристики')

    class Meta:
        verbose_name = 'Значение характеристики'
        verbose_name_plural = 'Значения характеристик '

    def __str__(self):
        return f'{self.description.name}: {self.value}'


class Subscribers(models.Model):
    email = models.EmailField(unique=True, verbose_name='E-mail')
    first_name = models.CharField(max_length=100, verbose_name='Имя')

    class Meta:
        verbose_name = 'Подписчик'
        verbose_name_plural = 'Подписчики'

    def __str__(self):
        return self.first_name

