import os
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.views.generic import DetailView, ListView
from rest_framework.generics import ListCreateAPIView
from main.models import *
from main.forms import *
from django.core.mail import send_mail
from rest_framework.response import Response
from rest_framework.views import APIView
from main.serializers import AdvertSerializer, CategorySerializer
from .tasks import send_email_subscribers


class AdvertView(APIView):
    def get(self, request):
        advert = Advert.objects.all()
        serializer = AdvertSerializer(advert, many=True)
        return Response(serializer.data)


class CategoryList(ListCreateAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class HomeView(ListView):
    model = Advert
    template_name = 'main/index.html'
    context_object_name = 'adverts'


class AdvertDetailView(DetailView):
    model = Advert
    template_name = 'main/advert_detail.html'
    context_object_name = 'adv'


# def filter_by_categories(request, category_pk):
#     category = Category.objects.get(pk=category_pk)
#     adverts = category.advert_set.all()
#     return render(request, 'main/filter_by_categories.html',
#                   {'category': category, 'adverts': adverts, })

class FilterCatView(ListView):
    model = Advert
    template_name = 'main/filter_by_categories.html'
    context_object_name = 'adverts'

    def get_queryset(self):
        cat = Category.objects.get(slug=self.kwargs['cat_slug'])
        return Advert.objects.filter(category__in=cat.get_children()) if cat.get_children() else Advert.objects.filter(
            category__slug=self.kwargs['cat_slug'])


@login_required(login_url='/user/login/')
def choose_category(request):
    categories = Category.objects.all()
    content = {'categories': categories}
    return render(request, 'main/choose_category.html', content)


@login_required(login_url='/user/login/')
def create_advert(request, slug):
    category = Category.objects.get(slug=slug)
    characteristics = category.categorydescription_set.all()
    if request.method == 'POST':
        form = AdvertForm(request.POST)
        gallery = request.FILES.getlist('gallery')
        content = {'category': category, 'form': form, 'characteristics': characteristics}
        if form.is_valid():
            adv = form.save(commit=False)
            adv.author = CustomUser.objects.get(pk=request.user.id)
            adv.category = category
            adv.save()

            for photo in gallery:
                Gallery.objects.create(advert=adv, image=photo)

            for ch in characteristics:
                if request.POST.get(ch.name) != '<не указано>':
                    DescriptionValue.objects.create(advert=adv, description=ch, value=request.POST.get(ch.name))

            return redirect('home')
    else:
        form = AdvertForm()
        content = {'category': category, 'form': form, 'characteristics': characteristics}
    return render(request, 'main/create_advert.html', content)


def see_about(request):
    if request.method == 'POST':
        form = SubscribersForm(request.POST)
        if form.is_valid():
            form.save()
            form = SubscribersForm()
            send_email_subscribers.delay(request.POST.get('email'))
    else:
        form = SubscribersForm()

    return render(request, 'main/see_about.html', {'form': form})
