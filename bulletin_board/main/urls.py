from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

import main.views
from main.views import *

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('advert/<int:pk>/', AdvertDetailView.as_view(), name='advert_detail'),
    path('category/<str:cat_slug>', FilterCatView.as_view(), name='filter_by_categories'),
    path('create_advert/', choose_category, name='choose_category'),
    path('create_advert/<str:slug>', create_advert, name='create_advert'),
    path('see_about', see_about, name='see_about'),
    path('api_advert/', main.views.AdvertView.as_view(), name='api_advert'),
    path('api_category/', main.views.CategoryList.as_view(), name='api_category'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
