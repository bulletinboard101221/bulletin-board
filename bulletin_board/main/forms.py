from django.forms import ModelForm
from main.models import *


class AdvertForm(ModelForm):
    class Meta:
        model = Advert
        fields = ('title', 'content', 'price', 'address')


class SubscribersForm(ModelForm):
    class Meta:
        model = Subscribers
        fields = ('first_name', 'email')
