from celery.schedules import crontab
from django.core.mail import send_mail
from bulletin_board.celery import app
from users.models import CustomUser


@app.task
def send_email_subscribers(user_email):
    send_mail('Подписка',
              'Спасибо! Вы подписались на получение новостей от нашего сайта! Вы будете первым узнавать о новых функциях нашего сайте',
              'bulletinboard101221@gmail.com',
              [user_email, ])


@app.task
def send_hello_email():
    users = CustomUser.objects.all()
    if users:
        send_mail(
            'Bulletin board',
            'Посмотрите новые объявления на нашем сайте!',
            'bulletinboard101221@gmail.com',
            [user.email for user in users],
            fail_silently=False,
        )


app.conf.beat_schedule = {
    'regular-email': {
        'task': 'main.tasks.send_hello_email',
        'schedule': crontab(minute='*/5'),
    },
}
