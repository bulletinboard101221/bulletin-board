from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.utils import timezone
from django.utils.safestring import mark_safe
from .managers import CustomUserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True, verbose_name='E-mail')
    first_name = models.CharField(max_length=100, verbose_name='Имя')
    last_name = models.CharField(max_length=100, verbose_name='Фамилия')
    phone_number = models.CharField(max_length=15, verbose_name='Номер телефона')
    photo = models.ImageField(upload_to='user_photos/%Y/%m/%d', verbose_name='Фото пользователя', blank=True, null=True)
    is_staff = models.BooleanField(default=False, verbose_name='Персонал сайта')
    is_active = models.BooleanField(default=True, verbose_name='Активные пользователи')
    date_joined = models.DateTimeField(default=timezone.now)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def photo_show(self):
        return mark_safe(f'<img src="{self.photo.url}" width="200" height="150"/>') if self.photo else None

    photo_show.__name__ = "Предпросмотр"

    def __str__(self):
        return '%s %s %s' % (self.first_name, self.last_name, self.email)

