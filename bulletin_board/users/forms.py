from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser
from django.forms import ModelForm
from main.models import *
from django import forms



class CustomUserCreationForm(UserCreationForm):
    password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    class Meta:
        model = CustomUser
        fields = ('first_name', 'last_name', 'email', 'phone_number')


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ('first_name', 'last_name', 'email', 'phone_number')


class UserUpdateForm(forms.Form):
    photo = forms.ImageField(label='Фото профиля', required=False)
    first_name = forms.CharField(label='Имя', max_length=100, )
    last_name = forms.CharField(label='Фамилия', max_length=100)
    email = forms.EmailField(label='Email', )
    phone_number = forms.CharField(label='Номер телефона', max_length=15)


class AdvertUpdateForm(forms.Form):
    title = forms.CharField(max_length=100, label='Название')
    content = forms.CharField(widget=forms.Textarea, label='Описание')
    price = forms.DecimalField(decimal_places=2, max_digits=10, label='Цена')
    address = forms.CharField(max_length=200, label='Адрес')
