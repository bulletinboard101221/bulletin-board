from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ('last_name', 'first_name', 'email', 'is_staff', 'is_active',)
    list_display_links = ('email',)
    list_filter = ('is_staff', 'is_active',)
    fieldsets = (
        (None, {'fields': ('first_name', 'last_name', 'email', 'phone_number', 'photo', 'photo_show')}),
        ('Permissions', {'fields': ('is_staff', 'is_active')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'is_staff',
                       'is_active')}
         ),
    )
    search_fields = ('last_name',)
    ordering = ('last_name',)
    readonly_fields = ('photo_show',)


admin.site.register(CustomUser, CustomUserAdmin)
