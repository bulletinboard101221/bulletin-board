from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.views import LoginView
from django.core.files.storage import FileSystemStorage
from django.shortcuts import render, redirect
from django.utils.safestring import mark_safe
from users.forms import CustomUserCreationForm, UserUpdateForm
from django.views.generic import DetailView, ListView, DeleteView
from django.urls import reverse_lazy
from users.forms import AdvertUpdateForm
from main.models import *


def registration(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('home')
    else:
        form = CustomUserCreationForm()
    return render(request, 'users/registration.html', {'form': form})


class LoginUser(LoginView):
    form_class = AuthenticationForm
    template_name = 'users/login.html'


def view_account(request):
    return render(request, 'users/personal_account.html', {})


def add_favorites(request, advert_pk):
    if request.method == 'POST':
        if request.session.get('liked') is None:
            request.session['liked'] = []

    if advert_pk not in request.session['liked']:
        request.session['liked'].append(advert_pk)
        request.session.modified = True

    return redirect(request.META.get('HTTP_REFERER'))


def remove_favorites(request, advert_pk):
    if request.method == 'POST':
        if advert_pk in request.session['liked']:
            request.session['liked'].remove(advert_pk)
            if not len(request.session['liked']):
                del request.session['liked']

    request.session.modified = True
    return redirect(request.META.get('HTTP_REFERER'))


def favorites_advert_user_view(request):
    # context = {}
    # liked = request.session.get('liked')
    # if liked:
    #     context['adverts'] = Advert.objects.filter(pk__in=request.session['liked'])
    adverts = Advert.objects.filter(pk__in=request.session.get('liked', []))
    return render(request, 'users/favorites_advert.html', {'adverts': adverts})


def my_data(request):
    files = CustomUser.objects.get(pk=1).photo
    return render(request, 'users/my_data.html', {'f': files})


def update_my_data(request):
    update = True
    form = UserUpdateForm(request.POST)

    if request.method == 'POST':
        form = UserUpdateForm(request.POST, request.FILES)
        if form.is_valid():
            if form.cleaned_data['photo']:
                request.user.photo = form.cleaned_data['photo']
            request.user.first_name = form.cleaned_data['first_name']
            request.user.last_name = form.cleaned_data['last_name']
            request.user.email = form.cleaned_data['email']
            request.user.phone_number = form.cleaned_data['phone_number']

            print(form.cleaned_data)
            request.user.save()
    else:
        default_data = {'first_name': request.user.first_name, 'last_name': request.user.last_name,
                        'email': request.user.email, 'phone_number': request.user.phone_number}
        form = UserUpdateForm(default_data)

    return render(request, 'users/my_data.html', {'update': update, 'form': form})


class AuthoredAdsView(ListView):
    model = Advert
    template_name = 'users/authored_ads.html'
    context_object_name = 'adverts'

    def get_queryset(self):
        return Advert.objects.filter(author=self.request.user.id)


def update_advert(request, advert_pk):
    adv = Advert.objects.get(pk=advert_pk)
    category = adv.category
    values = adv.descriptionvalue_set.all()
    gallery = request.FILES.getlist('gallery')
    if request.method == 'POST':
        form = AdvertUpdateForm(request.POST)
        if form.is_valid():
            adv.title = form.cleaned_data['title']
            adv.content = form.cleaned_data['content']
            adv.price = form.cleaned_data['price']
            adv.address = form.cleaned_data['address']
            adv.save()

            for val in values:
                val.value = request.POST.get(val.description.name)
                val.save()

            for photo in adv.gallery_set.all():
                if not request.POST.get(str(photo.id)):
                    photo.delete()

            for photo in gallery:
                Gallery.objects.create(advert=adv, image=photo)

            return redirect('authored_ads')
    else:
        default_data = {'title': adv.title, 'content': adv.content,
                        'price': adv.price, 'address': adv.address}
        form = AdvertUpdateForm(default_data)

    return render(request, 'users/update_advert.html',
                  {'form': form, 'category': category, 'values': values, 'adv': adv})


class AdvertDeleteView(DeleteView):
    model = Advert
    template_name = 'users/delete_advert.html'
    success_url = reverse_lazy('authored_ads')
