from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import LogoutView
from django.urls import path
from users import views
from users.views import *

urlpatterns = [
    path('registration/', registration, name='registration'),
    path('login/', views.LoginUser.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('personal_account/', view_account, name='viewaccount'),
    path('favorites/<int:advert_pk>/', add_favorites, name='add_favorites'),
    path('remove_favorites/<int:advert_pk>/', remove_favorites, name='remove_favorites'),
    path('favorites_advert/', favorites_advert_user_view, name='favorites_advert'),
    path('my_data/', my_data, name='my_data'),
    path('update_my_data/', update_my_data, name='update_my_data'),
    path('authored_ads/', AuthoredAdsView.as_view(), name='authored_ads'),
    path('authored_ads/<int:advert_pk>/update/', update_advert, name='update_advert'),
    path('authored_ads/<int:pk>/delete/', AdvertDeleteView.as_view(), name='delete_advert'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
