from django.test import TestCase
from django.utils import timezone
from main.models import Advert, Category
from users.models import CustomUser


# Мы не можем получить поле verbose_name напрямую через advert.author.verbose_name, потому что advert.author является
# строкой. Вместо этого, нам надо использовать атрибут _meta объекта Advert для получения того экземпляра поля,
# который будет использоваться для получения дополнительной информации.

# Мы выбрали метод assertEquals(field_label, 'Автор') вместо  assertTrue(field_label == 'Автор'),
# потому что, в случае провала теста, в выводе будет указано какое именно значение содержит метка
# и это немного облегчит нам задачу по отладке кода.


class AdvertModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        CustomUser.objects.create()
        Category.objects.create()
        Advert.objects.create(author=CustomUser.objects.get(id=1),
                              title='Продам дом',
                              category=Category.objects.get(id=1),
                              content='Продам большой дом',
                              price=1000000,
                              publication_datetime=str(timezone.now),
                              edit_datetime=str(timezone.now),
                              address='г. Москва, ул. Первая, д. 23')

    def test_author_label(self):
        advert = Advert.objects.get(id=1)
        field_label = advert._meta.get_field('author').verbose_name
        self.assertEquals(field_label, 'Автор')

    def test_title_label(self):
        advert = Advert.objects.get(id=1)
        field_label = advert._meta.get_field('title').verbose_name
        self.assertEquals(field_label, 'Название')

    def test_title_max_length(self):
        advert = Advert.objects.get(id=1)
        max_length = advert._meta.get_field('title').max_length
        self.assertEquals(max_length, 100)

    def test_category_label(self):
        advert = Advert.objects.get(id=1)
        field_label = advert._meta.get_field('category').verbose_name
        self.assertEquals(field_label, 'Категория')

    def test_content_label(self):
        advert = Advert.objects.get(id=1)
        field_label = advert._meta.get_field('content').verbose_name
        self.assertEquals(field_label, 'Описание объявления')

    def test_price_label(self):
        advert = Advert.objects.get(id=1)
        field_label = advert._meta.get_field('price').verbose_name
        self.assertEquals(field_label, 'Цена')

    def test_price_decimal_places(self):
        advert = Advert.objects.get(id=1)
        decimal_places = advert._meta.get_field('price').decimal_places
        self.assertEquals(decimal_places, 2)

    def test_price_max_digits(self):
        advert = Advert.objects.get(id=1)
        max_digits = advert._meta.get_field('price').max_digits
        self.assertEquals(max_digits, 10)

    def test_publication_datetime_label(self):
        advert = Advert.objects.get(id=1)
        field_label = advert._meta.get_field('publication_datetime').verbose_name
        self.assertEquals(field_label, 'Дата публикации')

    def test_publication_datetime_auto_now_add(self):
        advert = Advert.objects.get(id=1)
        auto_now_add = advert._meta.get_field('publication_datetime').auto_now_add
        self.assertEquals(auto_now_add, True)

    def test_edit_datetime_label(self):
        advert = Advert.objects.get(id=1)
        field_label = advert._meta.get_field('edit_datetime').verbose_name
        self.assertEquals(field_label, 'Дата изменения')

    def test_edit_datetime_auto_now_add(self):
        advert = Advert.objects.get(id=1)
        auto_now = advert._meta.get_field('edit_datetime').auto_now
        self.assertEquals(auto_now, True)

    def test_address_label(self):
        advert = Advert.objects.get(id=1)
        field_label = advert._meta.get_field('address').verbose_name
        self.assertEquals(field_label, 'Адрес')

    def test_address_max_length(self):
        advert = Advert.objects.get(id=1)
        max_length = advert._meta.get_field('address').max_length
        self.assertEquals(max_length, 200)

    def test_likes_qty_label(self):
        advert = Advert.objects.get(id=1)
        field_label = advert._meta.get_field('likes_qty').verbose_name
        self.assertEquals(field_label, 'Кол-во лайков')

    def test_likes_qty_blank(self):
        advert = Advert.objects.get(id=1)
        field_label = advert._meta.get_field('likes_qty').blank
        self.assertEquals(field_label, True)

    def test_likes_qty_null(self):
        advert = Advert.objects.get(id=1)
        field_label = advert._meta.get_field('likes_qty').null
        self.assertEquals(field_label, True)

    def test_model_label(self):
        model_label = Advert._meta.verbose_name.title()
        self.assertEquals(model_label, 'Объявление')

    def test_model_label_plural(self):
        model_label_plural = Advert._meta.verbose_name_plural.title()
        self.assertEquals(model_label_plural, 'Объявления')

    def test_model_ordering(self):
        model_ordering = Advert._meta.ordering
        self.assertEquals(model_ordering, ['-publication_datetime'])

    def test_object_name_is_title(self):
        advert = Advert.objects.get(id=1)
        expected_object_name = advert.title
        self.assertEquals(expected_object_name, str(advert))
