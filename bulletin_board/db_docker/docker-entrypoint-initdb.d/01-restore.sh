file="/docker-entrypoint-initdb.d/pg_dump.pgdata"
dbname=bulletin_board

pg_restore -U admin --dbname=$dbname --verbose --single-transaction < "$file" || exit 1